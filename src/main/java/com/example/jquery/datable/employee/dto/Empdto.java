package com.example.jquery.datable.employee.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Empdto {

	private long id;
	private String name;
	private String lastName;
	private String email;
	private String phone;
	private boolean active;

}
