package com.example.jquery.datable.employee.cntlr;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.jquery.datable.employee.dto.Empdto;
import com.example.jquery.datable.employee.service.EmpService;

@Controller
@RequestMapping("/web")
public class EmpCntlr {

	@Autowired
	private EmpService svc;

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String goHome() {
		return "home";
	}

	
	@RequestMapping(path="/employees", method=RequestMethod.GET,produces = "application/json")
	public @ResponseBody List<Empdto> getAllEmployees(){
		return svc.getAllEmployees();
	}
}
