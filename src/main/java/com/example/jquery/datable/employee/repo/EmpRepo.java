package com.example.jquery.datable.employee.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.jquery.datable.employee.entity.Emp;

public interface EmpRepo extends JpaRepository<Emp, Long> {

}
