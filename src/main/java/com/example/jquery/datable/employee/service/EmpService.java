package com.example.jquery.datable.employee.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.jquery.datable.employee.dto.Empdto;
import com.example.jquery.datable.employee.entity.Emp;
import com.example.jquery.datable.employee.repo.EmpRepo;

@Service
public class EmpService {

	@Autowired
	private EmpRepo repo;

	public List<Empdto> getAllEmployees() {
			System.out.println(repo.findAll());
		return repo.findAll().stream().map(this::convert).collect(Collectors.toList());
	}

	public Empdto convert(Emp emp) {
		return new Empdto(emp.getId(), emp.getName(), emp.getLastName(), emp.getEmail(), emp.getPhone(),
				emp.getActive());
	}

}
